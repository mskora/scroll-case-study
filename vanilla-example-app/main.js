import './style.css';

// document.querySelector('#app').innerHTML = `
//   <h1>Hello Vite!</h1>
//   <a href="https://vitejs.dev/guide/features.html" target="_blank">Documentation</a>
// `
const sections = document.querySelectorAll('section');

sections.forEach((section) => {
  console.log(section.style.backgroundColor);
});

function handleClick(e) {
  e.preventDefault();

  const href = e.target.getAttribute('href');
  const offsetTop = document.querySelector(href).offsetTop;

  scroll({
    top: offsetTop,
    behavior: 'smooth',
  });
}

const links = document.querySelectorAll('a');

for (const link of links) {
  link.addEventListener('click', handleClick);
}
